﻿using System;

namespace comp5002_10009622_assessment01
{
    class Program
    {
        static void Main(string[] args)
        {
        // Declaring Variables
            var name1 = ""; // giving a variable a blank value
            var dec_num = 0.00; // declaring the number to 2 decimal places
            var decision = ""; // giving a varible a blank value


            Console.WriteLine("Welcome to my shop"); // greeting to the shop
            Console.WriteLine();
            Console.WriteLine("What is your name"); // asking the user for their name
                name1 = (Console.ReadLine()); // customer first name 
   
            Console.Clear(); // clearing all previous lines in the console 
            Console.WriteLine("Please type in a number with 2 decimal places");// asking the user to choose a number
            dec_num = double.Parse(Console.ReadLine()); // choosing first 2 decimal number
            Console.WriteLine("Do you want another number? type yes/no");// asking the user a yes/no question

                decision = Console.ReadLine(); // defining value through user input 
            if(decision == "yes") // if the user input = "yes" then ask for another decimal, then add variable1 and variable2 and add GST(0.15,15%)
            {

            Console.WriteLine("Please type another number with 2 decimal places");
            dec_num += double.Parse(Console.ReadLine()); // Defining variable through user input variable2
            Console.WriteLine($" Your total has come to {(dec_num)*1.15:C3} Including GST");// display variable1+varible2 including GST 

            }
            else if(decision == "no")// If the user input = "no" then display variable1 and add GST 
            {

                Console.WriteLine($" Your total has come to {dec_num*1.15:C3} Including GST");// display variable1 including GST

            }


            Console.WriteLine("Thank you for shopping with us, please come again"); // saying goodbye to customer and thanking them for shopping with us 
            Console.WriteLine();// making a blank line 


            Console.WriteLine("Press <ENTER> to Close the Application");
            
            ConsoleKeyInfo keyInfo = Console.ReadKey();
            while(keyInfo.Key != ConsoleKey.Enter)
                keyInfo = Console.ReadKey();// Application will only close if <ENTER> is pressed 
        }
    }
}